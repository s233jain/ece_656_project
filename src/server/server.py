import socket
import sys
import mysql.connector
from mysql.connector import errorcode
from time import sleep
import pickle
import traceback

config = {
  'user': '',
  'password': '',
  'host': 'marmoset02.shoshin.uwaterloo.ca',
  'database': 'db656_s233jain',
  'raise_on_warnings': True
}

def connect_to_db(config):
    return mysql.connector.connect(**config)

if __name__=="__main__":

    try:
        cnx = connect_to_db(config)
        cnx.raise_on_warnings = False
        cursor = cnx.cursor()

        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind(('', 11111))

        server_socket.listen(1)

        while True:
            client, addr = server_socket.accept()
            query = client.recv(1024).decode("utf-8")
            print("Received from client: " + str(query))

            cursor.execute(query)
            query_res = cursor.fetchall()
            if not len(query_res):
                send_res=pickle.dumps("QUERY_FAILED")
                client.sendall(send_res)
            else:
                print("Received from DB query:" + str(query_res))
                data=pickle.dumps(query_res)
                client.sendall(data)

        server_socket.close()
        cnx.close()

    except Exception as e:
        print("Exception Raised!")
        traceback.print_exc()
        server_socket.close()
        cnx.close()
    except KeyboardInterrupt as ex:
        pass
