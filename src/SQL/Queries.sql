-- Top N popular artists
SELECT Name,Follower_Count,Popularity_Score
FROM Artist
ORDER BY Popularity_Score DESC
LIMIT 10;

-- Top N most followed artists
SELECT Name,Follower_Count,Popularity_Score
FROM Artist
ORDER BY Follower_Count DESC
LIMIT 10;

-- Show distinct list of artists by names
SELECT Name
FROM Artist
ORDER BY Name;
--LIMIT 50;

-- Top N party songs (using loudness*danceability*energy)
SELECT T.Name,T.Track_href,T.Duration,T.Popularity
FROM Track AS T
WHERE T.Danceability>(SELECT AVG(Danceability) FROM Track) AND T.Energy>(SELECT AVG(Energy) FROM Track) AND T.Loudness>(SELECT AVG(Loudness) FROM Track)
		AND T.Danceability+T.Energy+T.Loudness>=(
													SELECT MIN(P.PartyScore)
													FROM (
														SELECT Ranking.PartyScore
														FROM (
															SELECT Tr.Danceability+Tr.Energy+Tr.Loudness AS PartyScore,@counter:=@counter+1 AS counter
															FROM (select @counter:=0) AS initvar,Track AS Tr
															ORDER BY PartyScore DESC
															) AS Ranking
														WHERE Ranking.counter <= (15/100 * @counter)
														) AS P
												)
ORDER BY T.Popularity
LIMIT 10;

-- Top N melow songs (using Acousticness*instrumentalness)
SELECT T.Name,T.Track_href,T.Duration,T.Popularity
FROM Track AS T
WHERE T.Acousticness>(SELECT AVG(Acousticness) FROM Track) AND T.Instrumentalness>(SELECT AVG(Instrumentalness) FROM Track)
		AND T.Acousticness+T.Instrumentalness>=(
													SELECT MIN(P.MelowScore)
													FROM (
														SELECT Ranking.MelowScore
														FROM (
															SELECT Tr.Acousticness+Tr.Instrumentalness AS MelowScore,@counter:=@counter+1 AS counter
															FROM (select @counter:=0) AS initvar,Track AS Tr
															ORDER BY MelowScore DESC
															) AS Ranking
														WHERE Ranking.counter <= (15/100 * @counter)
														) AS P
												)
ORDER BY T.Popularity
LIMIT 10;

-- Top N rap songs (using Tempo*Speechiness)
SELECT T.Name,T.Track_href,T.Duration,T.Popularity
FROM Track AS T
WHERE T.Speechiness>(SELECT AVG(Speechiness) FROM Track) AND T.Tempo>(SELECT AVG(Tempo) FROM Track)
		AND T.Speechiness+T.Tempo>=(
													SELECT MIN(P.RapScore)
													FROM (
														SELECT Ranking.RapScore
														FROM (
															SELECT Tr.Speechiness+Tr.Tempo AS RapScore,@counter:=@counter+1 AS counter
															FROM (select @counter:=0) AS initvar,Track AS Tr
															ORDER BY RapScore DESC
															) AS Ranking
														WHERE Ranking.counter <= (15/100 * @counter)
														) AS P
												)
ORDER BY T.Popularity
LIMIT 10;

-- Show albums for the top N popular artists
SELECT TopArtists.Name AS 'Artist_Name', Alb.Name AS 'Album_Title'
FROM (SELECT Ar.Name,Ar.ArtistID FROM Artist AS Ar ORDER BY Ar.Popularity_Score DESC /*LIMIT 5*/) AS TopArtists
	INNER JOIN Artist_Track AS A_T ON TopArtists.ArtistID=A_T.ArtistID
	INNER JOIN Track AS Tr ON A_T.TrackID=Tr.TrackID
	INNER JOIN Album AS Alb ON Tr.AlbumID = Alb.AlbumID
ORDER BY TopArtists.Name,Alb.Name;

-- Show songs of a specific album
SELECT Album.Name AS 'Album_Title', Track.Name AS 'Track_Name'
FROM Track INNER JOIN Album ON Track.AlbumID = Album.AlbumID
WHERE Album.Name='ALBUM_NAME';
--LIMIT 100;

-- Show songs of a specific artist
SELECT Tr.Name AS 'Track_Name'
FROM (SELECT Artist.ArtistID FROM Artist WHERE Artist.Name='ARTIST_NAME') AS Selected_Artist
	INNER JOIN Artist_Track AS A_T ON Selected_Artist.ArtistID=A_T.ArtistID
	INNER JOIN Track AS Tr ON A_T.TrackID=Tr.TrackID
ORDER BY Tr.Name;
--LIMIT 100;

-- Show top-10 most popular albums originated from a specific country
SELECT Album.Name AS 'Album_Title'
FROM (SELECT Track.Popularity, Track.AlbumID FROM Track INNER JOIN Country ON Track.Origin_CountryID = Country.CountryID WHERE Country.Name='COUNTRY_NAME') AS TC
	INNER JOIN Album ON TC.AlbumID = Album.AlbumID
GROUP BY Album.AlbumID
ORDER BY AVG(TC.Popularity) DESC
LIMIT 10;