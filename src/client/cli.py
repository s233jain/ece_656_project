import cmd
import socket
from time import sleep
from decimal import Decimal
import pickle
import sys
import re

class ClientCli(cmd.Cmd):
    prompt='(spotify-client) '
    top_sub_cmds_list = ['song', 'party', 'rap', 'mellow', 'album', 'artist', 'followed_artist', 'origin_country']
    show_sub_cmds_list = ['artist', 'album', 'origin_country']

    top_n_popular_artists = r'top (.*) artist'
    top_n_followed_artists = r'top (.*) followed_artist'
    top_n_party_songs = r'top (.*) party songs'
    top_n_rap_songs = r'top (.*) rap songs'
    top_n_mellow_songs = r'top (.*) mellow songs'
    top_album_country = r'top album origin_country (.*)'
    show_artists = r'show artist'
    show_origin_country = r'show origin_country'
    show_albums_top_artists = r'show album top (.*) artist'
    show_song_album = r'show song album (.*)'
    show_song_artist = r'show song artist (.*)'

    def cmd_matcher(self, cmd):
        matching = re.match(self.top_n_popular_artists, cmd)
        if matching and int(matching.group(1)) < 100:
            return "SELECT Name FROM Artist ORDER BY Popularity_Score DESC LIMIT " + matching.group(1)

        matching = re.match(self.top_n_followed_artists, cmd)
        if matching and int(matching.group(1)) < 100:
            return "SELECT Name, Follower_Count, Popularity_Score FROM Artist ORDER BY Follower_Count DESC LIMIT " + matching.group(1)

        matching = re.match(self.top_n_party_songs, cmd)
        if matching and int(matching.group(1)) < 100:
            return "SELECT T.Name,T.Track_href,T.Duration,T.Popularity FROM Track AS T WHERE T.Danceability>(SELECT AVG(Danceability) FROM Track) AND T.Energy>(SELECT AVG(Energy) FROM Track) AND T.Loudness>(SELECT AVG(Loudness) FROM Track) AND T.Danceability+T.Energy+T.Loudness>=( SELECT MIN(P.PartyScore) FROM ( SELECT Ranking.PartyScore FROM ( SELECT Tr.Danceability+Tr.Energy+Tr.Loudness AS PartyScore,@counter:=@counter+1 AS counter FROM (select @counter:=0) AS initvar,Track AS Tr ORDER BY PartyScore DESC ) AS Ranking WHERE Ranking.counter <= (15/100 * @counter) ) AS P ) ORDER BY T.Popularity LIMIT " + matching.group(1)

        matching = re.match(self.top_n_rap_songs, cmd)
        if matching and int(matching.group(1)) < 100:
            return "SELECT T.Name,T.Track_href,T.Duration,T.Popularity FROM Track AS T WHERE T.Speechiness>(SELECT AVG(Speechiness) FROM Track) AND T.Tempo>(SELECT AVG(Tempo) FROM Track) AND T.Speechiness+T.Tempo>=( SELECT MIN(P.RapScore) FROM ( SELECT Ranking.RapScore FROM ( SELECT Tr.Speechiness+Tr.Tempo AS RapScore,@counter:=@counter+1 AS counter FROM (select @counter:=0) AS initvar,Track AS Tr ORDER BY RapScore DESC ) AS Ranking WHERE Ranking.counter <= (15/100 * @counter) ) AS P ) ORDER BY T.Popularity LIMIT " + matching.group(1)

        matching = re.match(self.top_n_mellow_songs, cmd)
        if matching and int(matching.group(1)) < 100:
            return "SELECT T.Name,T.Track_href,T.Duration,T.Popularity FROM Track AS T WHERE T.Acousticness>(SELECT AVG(Acousticness) FROM Track) AND T.Instrumentalness>(SELECT AVG(Instrumentalness) FROM Track) AND T.Acousticness+T.Instrumentalness>=( SELECT MIN(P.MelowScore) FROM ( SELECT Ranking.MelowScore FROM ( SELECT Tr.Acousticness+Tr.Instrumentalness AS MelowScore,@counter:=@counter+1 AS counter FROM (select @counter:=0) AS initvar,Track AS Tr ORDER BY MelowScore DESC ) AS Ranking WHERE Ranking.counter <= (15/100 * @counter) ) AS P ) ORDER BY T.Popularity LIMIT " + matching.group(1)

        matching = re.match(self.show_artists, cmd)
        if matching:
            return "SELECT Name FROM Artist ORDER BY Name LIMIT 50"

        matching = re.match(self.show_origin_country, cmd)
        if matching:
            return "SELECT Name FROM Country"

        matching = re.match(self.show_albums_top_artists, cmd)
        if matching:
            return "SELECT TopArtists.Name AS 'Artist_Name', Alb.Name AS 'Album_Title' FROM (SELECT Ar.Name,Ar.ArtistID FROM Artist AS Ar ORDER BY Ar.Popularity_Score DESC LIMIT " + matching.group(1) + ") AS TopArtists INNER JOIN Artist_Track AS A_T ON TopArtists.ArtistID=A_T.ArtistID INNER JOIN Track AS Tr ON A_T.TrackID=Tr.TrackID INNER JOIN Album AS Alb ON Tr.AlbumID = Alb.AlbumID ORDER BY TopArtists.Name,Alb.Name"
        matching = re.match(self.show_song_album, cmd)
        if matching:
            return "SELECT Album.Name AS 'Album_Title', Track.Name AS 'Track_Name' FROM Track INNER JOIN Album ON Track.AlbumID = Album.AlbumID WHERE Album.Name='" + matching.group(1) + "' LIMIT 100"

        matching = re.match(self.show_song_artist, cmd)
        if matching:
            return "SELECT Tr.Name AS 'Track_Name' FROM (SELECT Artist.ArtistID FROM Artist WHERE Artist.Name='"+ matching.group(1) +"') AS Selected_Artist INNER JOIN Artist_Track AS A_T ON Selected_Artist.ArtistID=A_T.ArtistID INNER JOIN Track AS Tr ON A_T.TrackID=Tr.TrackID ORDER BY Tr.Name LIMIT 100"

        matching = re.match(self.top_album_country, cmd)
        if matching:
            return "SELECT Album.Name AS 'Album_Title' FROM (SELECT Track.Popularity, Track.AlbumID FROM Track INNER JOIN Country ON Track.Origin_CountryID = Country.CountryID WHERE Country.Name='"+ matching.group(1) +"') AS TC INNER JOIN Album ON TC.AlbumID = Album.AlbumID GROUP BY Album.AlbumID ORDER BY AVG(TC.Popularity) DESC LIMIT 10"

        return

    def do_top(self, top_cmd):
        query_str = self.cmd_matcher("top " + top_cmd)
        if not query_str:
            print("Please provide a valid command!")
            return
        else:
            response = connect_to_server(query_str)
            if response != "QUERY_FAILED":
                pretty_print_op(response)
            else:
                print("Command execution encountered an error. Please retry or check server.")
 
    def complete_top(self, text, line, begidx, endidx):
        if not text:
            completions = self.top_sub_cmds_list[:]
        else:
            completions = [ sub_cmd
                            for sub_cmd in self.top_sub_cmds_list
                            if sub_cmd.startswith(text)
                            ]
        return completions

    def do_show(self, show_cmd):
        query_str = self.cmd_matcher("show " + show_cmd)
        if not query_str:
            print("Please provide a valid command!")
            return
        else:
            response = connect_to_server(query_str)
            if response != "QUERY_FAILED":
                pretty_print_op(response)
            else:
                print("Command execution encountered an error. Please retry or check server.")

    def complete_show(self, text, line, begidx, endidx):
        if not text:
            completions = self.show_sub_cmds_list[:]
        else:
            completions = [ sub_cmd
                            for sub_cmd in self.show_sub_cmds_list
                            if sub_cmd.startswith(text)
                            ]
        return completions

    def do_exit(self, line):
        return True

    def do_EOF(self, line):
        return True

def pretty_print_op(cmd_op):
    column_width=20
    for tup in cmd_op:
        tup_str=""
        for entry in tup:
            tup_str = tup_str + str(entry).ljust(column_width) + " "
        print(tup_str)

def connect_to_server(cmd):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('', 11111))

    client_socket.send(bytes(cmd, 'utf-8'))

    data = pickle.loads(client_socket.recv(8196))

    client_socket.close()
    return data
  
if __name__=="__main__":

    try:
        ClientCli().cmdloop()
    except Exception as e:
        print(e)
    except KeyboardInterrupt as keyexp:
        sys.exit(0) 
